# Projet Blog

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Spring Boot pour le backend et Angular pour le frontend.

## Fonctionnalités attendues
* Créer des articles
* Consulter la liste des articles
* Consulter un article spécifique
* Modifier ou supprimer un article

La gestion des users n'est pas attendue, on va considérer que tout le monde peut poster un article en mettant juste son nom dans le formulaire.

### Fonctionalités bonus 
* Ajouter une barre de recherche pour les articles
* Permettre des commentaires sur les posts
* Ajouter des catégories pour les articles
* Ajouter un compteur de vue sur les posts
* Faire des tests côté front et côté back

## Travail attendu
* Créer les wireframes des différentes pages de l'application (mobile first)
* Créer la base de données et les composants d'accès aux données pour la ou les tables avec JDBC (repository)
* Créer une API Rest avec Spring Boot (les contrôleurs)
* Créer le front (**responsive**) de l'application avec Angular
* Requêter l'API Rest depuis les services de Angular

